#!/bin/bash
yum update -y
yum install -y httpd
systemctl start httpd
systemctl enable httpd
mkdir var/www/html/orders/
echo "<h1>Orders instance ${instance_index} : $(hostname -f)</h1>" > /var/www/html/orders/index.html