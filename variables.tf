variable "availability-zone" {
  type    = string
  default = "us-east-1a"
}

variable "ec2-instance-ami" {
  type    = string
  default = "ami-06b09bfacae1453cb"
}

variable "ec2-instance-type" {
  type    = string
  default = "t2.micro"
}

variable "vpc-cidr-block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "main-subnet-cidr-block" {
  type    = string
  default = "10.0.1.0/24"
}

variable "main-subnet-az" {
  type    = string
  default = "us-east-1a"
}

variable "additional-subnet-cidr-block" {
  type    = string
  default = "10.0.2.0/24"
  description = "additional subnet needed for alb"
}

variable "additional-subnet-az" {
  type    = string
  default = "us-east-1b"
  description = "additional subnet needs to be in a different az as required for alb"
}

variable "orders-instance-count" {
  type    = number
  default = 3
  description = "number of EC2 instances that needs to created for orders service"
}

variable "payment-instance-count" {
  type    = number
  default = 3
  description = "number of EC2 instances that needs to created for orders service"
}