# AWS ALB infrastructure using terraform

The goal of this project is to create an infrastructure where you can dynamically assign number of servers (EC2 instances) according to the requirements. 
In general, there are two target groups which describes two endpoints ({url}/orders and {url}/payments) that would redirect to the group of EC2 instances that would serve the clients.

The following AWS services has been used in this project:
1. Application load balancer (2 target groups and 1 listener)
2. EC2 instances
3. Security groups

#### Note:
user_data for the EC2 instances had been passed to the aws_instance resource dynamically using the .tpl file.
The instance number belonging to a target group is being dynamically passed for us to confirm that even though we are sending to a specific endpoint, it get redirected to different EC2 instances within belonging to a target group

### Infrastructure diagram:

![Infrastructure](/diagram/infrastructure_diagram.png)

## Getting started

### Required libraries:
* Terraform
* AWS CLI

#### Note:
You need to add you IAM access key and secret access key to access your account and create the infrastructure.
If you don't want to configure/use the CLI, simply pass the access key, secret access key and region in the terraform providers.tf file.
Run the following commands once you clone the project:

```
terraform init
terraform plan
terraform apply --auto-approve
```

The infrastructure will be created. 
Look for the url link generated in EC2/load balancer/generated load balancer. 
You use this link in your browser to send GET request to the load balancer and according to the endpoint, it will redirect you to the associated EC2 instance

To destroy the infrastructure, run the following command:

```
terraform destroy --auto-approve
```
