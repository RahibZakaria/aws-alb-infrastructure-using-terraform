#!/bin/bash
yum update -y
yum install -y httpd
systemctl start httpd
systemctl enable httpd
mkdir var/www/html/payments/
echo "<h1>Payments instance ${instance_index} : $(hostname -f)</h1>" > /var/www/html/payments/index.html