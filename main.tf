######################
### orders servers ###
######################

resource "aws_instance" "orders" {
  count                  = var.orders-instance-count
  ami                    = var.ec2-instance-ami
  instance_type          = var.ec2-instance-type
  vpc_security_group_ids = [aws_security_group.EC2-SG.id]
  subnet_id              = aws_subnet.main.id

  user_data = templatefile("orders_user_data.tpl", {
    instance_index = count.index
  })

  tags = {
    Name = "EC2-orders-instance-${count.index}"
  }
}

# Target group
resource "aws_lb_target_group" "orders-tg" {
  name        = "orders-tg"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200,202"
    path                = "/orders/index.html"
    port                = "80"
    protocol            = "HTTP"
    unhealthy_threshold = 3
  }
}

# Target group attachments
resource "aws_lb_target_group_attachment" "example_attachment" {
  count            = length(aws_instance.orders)

  target_group_arn = aws_lb_target_group.orders-tg.arn
  target_id        = aws_instance.orders[count.index].id
  port             = 80
}

########################
### payments servers ###
########################

resource "aws_instance" "payments" {
  count                  = var.payment-instance-count
  ami                    = var.ec2-instance-ami
  instance_type          = var.ec2-instance-type
  vpc_security_group_ids = [aws_security_group.EC2-SG.id]
  subnet_id              = aws_subnet.main.id

  user_data = templatefile("payments_user_data.tpl", {
    instance_index = count.index
  })

  tags = {
    Name = "EC2-payments-instance-${count.index}"
  }
}

# Target group
resource "aws_lb_target_group" "payments-tg" {
  name        = "payments-tg"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200,202"
    path                = "/payments/index.html"
    port                = "80"
    protocol            = "HTTP"
    unhealthy_threshold = 3
  }
}

# Target group attachment
resource "aws_lb_target_group_attachment" "payments_tg_attachment" {
  count            = length(aws_instance.payments)

  target_group_arn = aws_lb_target_group.payments-tg.arn
  target_id        = aws_instance.payments[count.index].id
  port             = 80
}

#################################
### Application Load Balancer ###
#################################

resource "aws_lb" "main" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.ALB-SG.id]
  subnets            = [aws_subnet.main.id, aws_subnet.additional.id]

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

# Configuration for orders
resource "aws_lb_listener" "orders" {
  load_balancer_arn = aws_lb.main.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.orders-tg.arn
  }
}

# routing rule for orders endpoint
resource "aws_lb_listener_rule" "orders" {
  listener_arn = aws_lb_listener.orders.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.orders-tg.arn
  }

  condition {
    path_pattern {
      values = ["/orders*"]
    }
  }
}

# routing rule for payments endpoint
resource "aws_lb_listener_rule" "payments" {
  listener_arn = aws_lb_listener.orders.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.payments-tg.arn
  }

  condition {
    path_pattern {
      values = ["/payments*"]
    }
  }
}
